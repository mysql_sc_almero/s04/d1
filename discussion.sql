--Add new records

--Add artists
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


--Add albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Fearless",
	"2008-1-1",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Red",
	"2012-1-1",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"A Star Is Born",
	"2008-1-1",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Born This Way",
	"2011-1-1",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Purpose",
	"2015-1-1",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Believe",
	"2012-1-1",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Dangerous Woman",
	"2016-1-1",
	6
);




-- Add songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Fearless",
	213,
	"Country pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Love Story",
	246,
	"Country pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"State of Grace",
	253,
	"Rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Red",
	204,
	"Country",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Born This Way",
	252,
	"Electropop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Sorry",
	152,
	"Dancehall",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Into You",
	242,
	"EDM",
	9
);


--ADVANCE SELECTS

--Exclude records
SELECT * FROM songs WHERE id != 1;

--Greater than
SELECT * FROM songs WHERE id > 5;

--Less than
SELECT * FROM songs WHERE id < 7;

--OR (Different search conditions)
SELECT * FROM songs WHERE id = 1 OR song_name = "Red" OR length < 200;

--IN (1 Field only)
SELECT * FROM songs WHERE id IN (1, 8, 9);

--Find partial matches
--Start search from beggining of string
SELECT * FROM songs WHERE song_name LIKE "b%";
--End
SELECT * FROM songs WHERE song_name LIKE "%s";
--Middle
SELECT * FROM songs WHERE song_name LIKE "%a%";

--CASE SENSITIVE
--Upper / Lower
SELECT UPPER(song_name) FROM songs WHERE song_name = UPPER("red");


--SORT RECORDS
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

--SORT WITH LIMITS
SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

--Get distinct records (unique)
SELECT DISTINCT genre FROM songs;

--Count
SELECT COUNT(*) FROM songs WHERE genre = "Rock";



--TABLE JOINS CONCEPT
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

--LEFT JOIN - kasama yung null data
SELECT * FROM artists
	LEFT JOIN  albums ON artists.id = albums.artist_id;

--RIGHT JOIN
SELECT * FROM artists
	RIGHT JOIN albums ON artists.id = albums.artist_id;

--OUTER JOIN
SELECT * FROM artists
	LEFT OUTER JOIN albums ON artists.id = albums.artist_id;


--Mini Activity:
--In a single SELECT command, show ALL artist with their corresponding albums AND songs
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.id;